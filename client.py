import socket, pickle
import sys
import struct
import torr_2_pb2 as torr
address = ('localhost', 6005)
client_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
client_socket.connect(address)

messages = ["foobar", "barbaz", "bazquxfad", "Jimmy Carter"]

for s in messages:

    info = torr.FileInfo(hash=str.encode("test hash"), filename="test file name", size=10)

    client_socket.sendall(info.SerializeToString())

client_socket.close()

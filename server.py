import socket, pickle
import sys
import struct
import torr_2_pb2 as torr
address = ('localhost', 6005)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(address)

while True:
    print("Listening")

    data = server_socket.recv(4096)

    info = torr.FileInfo()
    info.ParseFromString(data)

    print(info)

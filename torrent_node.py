# C:\Users\marcu>protoc --proto_path=D:\REPO\GitHub\torrentprotobuff --python_out=D:\REPO\GitHub\torrentprotobuff D:\REPO\GitHub\torrentprotobuff/torr-2.proto
import math
import struct
import sys
from ctypes import sizeof

import torr_2_pb2 as torr
import hashlib, socket




class TorrClient:
    owner = ''
    index = 0
    port = 0
    hub_port = 0
    hub_host = ''
    client_socket = None
    def __init__(self, owner, index, port, hub_host='127.0.0.1', hub_port=5000):
        self.owner = owner
        self.index = index
        self.port = port
        self.hub_host = hub_host
        self.hub_port = hub_port

        self.client_socket =  self.get_conn()

    def get_conn(self):
        address = (self.hub_host, self.hub_port)
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect(address)
        return client_socket

    def send_msg(self,sock, msg):      ##############################           bun
        # Prefix each message with a 4-byte length (network byte order)

        s = msg.SerializeToString()
        msg = struct.pack('>I', len(s))
        sock.send(msg)
        sock.send(s)

    # def recvall(self, sock, n):
    #     # Helper function to recv n bytes or return None if EOF is hit
    #     data = bytearray()
    #     while len(data) < n:
    #         packet = sock.recv(n - len(data))
    #         if not packet:
    #             return None
    #         data.extend(packet)
    #     return data


    # def recv_msg(self,sock,msg):
    #     # Read message length and unpack it into an integer
    #     raw_msglen = self.recvall(sock, 4)
    #     if not raw_msglen:
    #         return None
    #     msglen = struct.unpack('>I', raw_msglen)[0]
    #     # Read the message data
    #     s= self.recvall(sock, msglen)
        return s



    def recv_msg(self,sock,msg):    ##############################           bun
        # Read message length and unpack it into an integer

        len_buf = sock.recv(4)

        msg_len = struct.unpack('>L', len_buf)[0]
        print('msg_len {0}'.format(msg_len))
        msg_buf = sock.recv(msg_len)
        print(msg_buf)

        msg.ParseFromString(msg_buf)
        return msg


    #     https://eli.thegreenplace.net/2011/08/02/length-prefix-framing-for-protocol-buffers

    def registration_request(self):
        req_req = torr.RegistrationRequest(owner=self.owner, index=self.index, port=self.port)
        message = torr.Message(type=torr.Message.Type.REGISTRATION_REQUEST, registrationRequest = req_req)

        #conn = self.get_conn()
        conn = self.client_socket
        #self.send_message(conn,message)
        self.send_msg(conn, message)
        recv_msg = torr.Message()
        recv_msg =  self.recv_msg(conn,recv_msg)

        if (recv_msg.registrationResponse.status == torr.Status.SUCCESS):
            print('\n Node: {0} connected'.format(self.owner + str(self.index)))
        else:
            print('\n Node: {0} failed connected'.format(self.owner + str(self.index)))


       # conn.close()

    source_port = None
    files = []
    filenames_to_contents = {}
    collaborator_nodes = None
    @staticmethod
    def send_chunk_request(node_address:tuple, file_hash:bytes, chunk_index:int, conn=None):
        pass
    def receive_search_request(self,msgm,conn):
        pass
    def local_search(self,regex):
        pass
    def receive_local_search_request(self,msg,conn=None):
        pass
    def receive_upload_request(self,msg=None,conn=None):

        req = torr.UploadRequest()
        msg = torr.Message(type=torr.Message.Type.UPLOAD_REQUEST, uploadRequest=req)

        #conn = self.get_conn()
        conn= self.client_socket
        recv_msg = self.recv_msg(conn, msg)

        #self.send_message(conn,message)
        # self.send_msg(conn, message)
        # recv_msg = torr.Message()
        # recv_msg =  self.recv_msg(conn,recv_msg)



    def receive_download_request(self,msg,conn):
        pass
    def receive_chunk_request(self,msg,conn=None):
        pass

    def send_subnet_request(self,subnet_id):
        pass

    def get_md5(chunk_content):
        pass

    @staticmethod
    def check_for_chunks_in_subnet(chunk_id,file_hash,collaboratos):
        if chunk_id is None:
            return [], {-1:[None,None]}
        node_status_list =[]     #######################################   413
        tried_nodes = 0
        found_chunk = False

        while found_chunk is False and tried_nodes != len(collaboratos):         ############################################   417
            collaboratos_index = (chunk_id +tried_nodes) % len(collaboratos)
            node = collaboratos[collaboratos_index]
            tried_nodes += 1  ###################################    420
            response, conn_established = TorrClient.send_chunk_request((node.host, node.port), file_hash, chunk_id)

            if response is None:
                replication_status = torr.Status.MESSAGE_ERROR
                replication_err_msg = 'Chunk response cannot be parsed'
            else:
                replication_status = response.chunkResponse.status
                replication_err_msg = response.chunkResponse.errorMessage
            if conn_established is False:   ############################   430
                replication_status = torr.Status.NETWORK_ERROR
                replication_err_msg = 'Chunk response: conn failded'
            node_replication_status = torr.NodeReplicationStatus(node=node, chunkIndex=chunk_id,status=replication_status,errorMessage=replication_err_msg)

            node_status_list.append(node_replication_status)
            if replication_status == torr.Status.SUCCESS:
                found_chunk = True
                chunk_content = response.chunkResponse.data
                chunk_hash = TorrClient.get_md5(chunk_content)     ##################################      441
                chunk = torr.ChunkInfo(index=chunk_id,size=len(chunk_content),hash=chunk_hash)

        if found_chunk is False:
            return node_status_list, {chunk_id:[None,node]}
        return node_status_list,{chunk_id:[chunk_content,chunk]}

    def receive_replicate_request(self,msg,conn=None):   # 376
        # Client -> Node ############################################## 450
        print("*"*30, "Receive replicate request","*"*30 )
        status = torr.Status.SUCCESS
        error_message = ''
        self.send_subnet_request(subnet_id=msg.replicateRequest.subnetId)
        found = False
        file_hash = msg.replicateRequest.fileInfo.hash
        file_size = msg.replicateRequest.fileInfo.size
        filename = msg.replicateRequest.fileInfo.filename

        with open('test.txt','a') as fp:
            fp.write(filename + f' replicated on port={self.source_port}\n') #######################################       460

        chunks = []
        node_status_list = []
        for file in self.files:       ############################ 391 / 443
            if file_hash == file.hash:
                found = True
                chunks = file.chunks
                break

        if not filename:
            status = torr.Status.MESSAGE_ERROR
            error_message = 'Filename cannot be empty'

        if not found:         ################################ 398
            try:
                file_content = b''
                self.filenames_to_contents[filename] = {'data':b'', 'chunks':[]}
                for i in range(math.ceil(file_size/1024)):
                    found_chunk = False
                    for node in self.collaborator_nodes:
                        if node.port == self.source_port:
                            continue
                        response, connection_established = self.send_chunk_request((node.host, node.port), file_hash=file_hash,chunk_index=i)
                        if response is None:
                            replication_status = torr.Status.MESSAGE_ERROR
                            replication_err_msg = 'Chunk response cannot be parsed'
                        else:  ###############################   413
                            replication_status = response.chunkResponse.status
                            replication_err_msg = response.chunkResponse.errorMessage
                        if connection_established is False:
                            replication_status = torr.Status.NETWORK_ERROR
                            replication_err_msg = 'Chunk response: connection failed'
                        node_replication_status = torr.NodeReplicationStatus(node=node, chunkIndex=i,status=replication_status,errorMessage=replication_err_msg)  ################################### 420

                        node_status_list.append(node_replication_status)
                        if replication_status == torr.Status.SUCCESS:
                            found_chunk = True
                            chunk_content = response.chunkResponse.data
                            self.filenames_to_contents[filename]['chunks'].append(chunk_content)

                            file_content += chunk_content
                            chunk_hash = self.get_md5(chunk_content)
                            chunk = torr.ChunkInfo(index=i, size=len(chunk_content),hash=chunk_hash)
                            chunks.append(chunk)
                            break
                    if found_chunk is False:  # 437
                        pass
            except Exception as e:
                status = torr.Status.PROCESSING_ERROR
                error_message = 'Processing error: ' + str(e)
        else:  ############### the file is already on the current state
            pass




if __name__ == "__main__":

    try:
        node_1 = TorrClient(owner='conn', index=1,port=5004)
        node_2 = TorrClient(owner='conn', index=2,port=5005)
        node_3 = TorrClient(owner='conn', index=3,port=5006)


        node_1.registration_request()
        node_2.registration_request()
        node_3.registration_request()


        node_1.receive_upload_request()
        node_2.receive_upload_request()
        node_3.receive_upload_request()
    except Exception as e:
        t = e